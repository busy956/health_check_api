package com.bj.healthcheckapi.entity;

import com.bj.healthcheckapi.enums.MachineType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MachineType machineType;

    @Column(nullable = false, length = 30)
    private String machineName;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private LocalDate dateBuy;

}
