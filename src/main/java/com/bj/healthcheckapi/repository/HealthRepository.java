package com.bj.healthcheckapi.repository;


import com.bj.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {
}
