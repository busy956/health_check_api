package com.bj.healthcheckapi.repository;

import com.bj.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine, Long> {
}
