package com.bj.healthcheckapi.controller;

import com.bj.healthcheckapi.model.MachineRequest;
import com.bj.healthcheckapi.model.MachineStaticsResponse;
import com.bj.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request) {
        machineService.setMachine(request);

        return "OK";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatic() {
        return machineService.getStatics();
    }
}
