package com.bj.healthcheckapi.controller;

import com.bj.healthcheckapi.model.HealthItem;
import com.bj.healthcheckapi.model.HealthRequest;
import com.bj.healthcheckapi.model.HealthResponse;
import com.bj.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {
        return healthService.getHealths();
    }

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id) {
        return healthService.getHealth(id);
    }
}
