package com.bj.healthcheckapi.service;

import com.bj.healthcheckapi.entity.Health;
import com.bj.healthcheckapi.model.HealthItem;
import com.bj.healthcheckapi.model.HealthRequest;
import com.bj.healthcheckapi.model.HealthResponse;
import com.bj.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth(HealthRequest request) {
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setIsChronicDisease(request.getIsChronicDisease());
        addData.setEtcMemo(request.getEtcMemo());

        healthRepository.save(addData);
    }

    public List<HealthItem> getHealths() {
        List<Health> originList = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originList) {  //향상된 for문
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setHealthStatus(health.getHealthStatus().getName());
            addItem.setIsGoHome(health.getHealthStatus().getIsGoHome());

            result.add(addItem);
        }

        return result;
    }

    public HealthResponse getHealth(long id) {
        Health originData = healthRepository.findById(id).orElseThrow();

        HealthResponse response = new HealthResponse();
        response.setId(originData.getId());
        response.setHealthStatusName(originData.getHealthStatus().getName());

        //삼항연산자 => 간단한 if
        response.setIsGoHomeName(originData.getHealthStatus().getIsGoHome() ? "예" : "아니오");

        response.setDateCreate(originData.getDateCreate());
        response.setName(originData.getName());

        response.setIsChronicDiseaseName(originData.getIsChronicDisease() ? "예" : "아니오");

        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }
}
