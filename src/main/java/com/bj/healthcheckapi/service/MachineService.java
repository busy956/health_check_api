package com.bj.healthcheckapi.service;

import com.bj.healthcheckapi.entity.Machine;
import com.bj.healthcheckapi.model.MachineRequest;
import com.bj.healthcheckapi.model.MachineStaticsResponse;
import com.bj.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics() {
        MachineStaticsResponse response = new MachineStaticsResponse();

        List<Machine> originList = machineRepository.findAll();

        double totalPrice = 0D;
        double totalpressKg = 0D;

        for(Machine machine : originList) {
            totalPrice += machine.getMachinePrice();
            totalpressKg += machine.getMachineType().getPressKg();
            // totalPrice = totalPrice + machine.getMachinePrice(); - java 연산자
        }

        double avgPrice = totalPrice / originList.size();

        response.setTotalPrice(totalPrice);
        response.setAveragePrice(avgPrice);
        response.setTotalPressKg(totalpressKg);

        return response;
    }
}
